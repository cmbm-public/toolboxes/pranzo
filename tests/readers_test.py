import pranzo.postprocess as p

sdir = "./testfiles"
file_id = "test"

meta = p.MetaReader(sdir=sdir, file_id=file_id)
antonpaar = p.AntonPaarReader(sdir=sdir, file_id=file_id)
tamair = p.TamAirReader(sdir=sdir, file_id=file_id)

def test_metareader():
    m = meta.get_meta_dict()
    assert m['calo']['temperature'] == '23C'
    assert m['setup']['system'] == 'OPC'


def test_APbase():
    antonpaar.get_data()

def test_APlines():
    lines = antonpaar.get_rheofile_corrected_lines()
    assert lines[0][-1] == ''
    assert lines[22][0] == '  Spaltabstand'
    assert lines[22][3] == 'd = 1 mm'
    assert len(lines[0]) == len(lines[-1])
    
def test_APdf():
    df_raw = antonpaar.rheofile_in_df()
    assert df_raw.iloc[0][0] == 'Arbeitsmappen Informationen'
    assert len(df_raw[1]) == 2354

    vnames = antonpaar.locate_variable_names(df_raw)
    assert list(vnames) == [39, 126, 163, 200, 237, 274, 323, 2152]

    df = antonpaar.set_df_headers(df_raw)
    assert list(df.columns) == ['messpkt','phase_time_s','time_s',
                                'storage_modulus_Pa','ratio_pc','shear_stress_Pa',
                                'shear_strain_pc','strain_rate_1ps','normal_force_N']


    col_expinfo = antonpaar.locate_col_with_expinfo(df)
    assert col_expinfo == 'storage_modulus_Pa'



def test_TAbase():
    tamair.get_data()

def test_TAdf():
    df_raw = tamair.calofile_in_df()
    assert list(df_raw.columns) == ['Time','Temperature',
                                'Heat flow','Heat',
                                'Normalized heat flow',
                                'Normalized heat','Time markers']
    
    df = tamair.rename_columns(df_raw)
    assert list(df.columns) == ['time', 'temperature',
                                'heat_flow', 'heat',
                                'normalized_heat_flow', 'normalized_heat',
                                'time_markers']