This file summarizes the experimental workflow to gather rheometry and calorimetry data.

# Rheometry
- Initialising the rheometer:
    1. Turn rheo and 2 water bath on. Open RheoPlus software.
    2. Click on the rheo icon. Steps to do: 
        1. Initialisieren (rheo and computer communicate together)
        2. After having set up the geometry and inserted the tool, Nullpunkt Setzen (sets zerogap)
        3. Normalkraft Rücksetzen and temperature to both cells Senden
        4. Speichern or OK (I don't remember)
- Setting up a protocol:
    1. Click on the (red and white) AIV icon
    2. Define the phases you want to work with. Max 35 phases. Important: for G' measurements, Messpunktdauer = 2s by f = 1 Hz. For strain sweep (exponential) and pre-shear, Messpunktdauer = 1s is fine
- Running an experiment:
    1. Click on the play icon
    2. Enter the experiment name
    3. Place the paste in the rheometer dans press Versuch jetzt starten
- Aborting/restarting an experiment:
    Either click on stop, or go the the protocol window and press stop in there. I prefer the second option because it gives more freedom.
    1. With the stop button: it will then ask you if you want to Mit Heben durchführen.
    2. With the rheo protocol stop: it will ask if you want to Mit Heben durchführen. If you want to start a measruement right away, say no, redefine your protocol and from the protocol window press start. It will ask for a new experiment name and then go on with the measurement.
    
- In case you cement the rheometer:
    1. Stop the measurement
    2. Disconnect the tool from the rheometer head
    3. Press the Online button on the rheometer to have the rheometer go offline (=not commanded by the computer)
    4. Press on the up arrow to bring the head up. The displacement is exponential over time, be patient at the beginning

- Exporting data:
    1. Click on the small table icon. WATCH OUT: you have 2 tables under this icon, the second one (which you want to export) hides behind the first one. Your target is the table with "Normalkraft, Schubspannung, Speichermodul, ect..."
    2. Go to file -> export -> content of active window -> Durchtrennung mit Komma (csv)
    3. Note: RheoPlus tends to round values when you export them, so make sure you have enough significant digits in the table
    4. If you exported the wrong rheo file (with Moment, Drehwinkel etc...), in csv_reader you need to uncomment some lines in get_rheo_indices and get_rheo_data


# Calorimetry
- Creating a new experiment + baseline
    1. File -> new experiment
    2. Set user (LM), experiment name (rough format = date_series_wc_admix_rheocalo)
    3. Select channel 
    4. Start baseline (30 min)
- Doing the experiment

    5. Insert vial in the calorimeter, record insertion time
    6. total sample mass = mass of binder (PCBM convention) = m_paste/(1+wc)
- Stopping the experiment

    7. Stop main part of experiment
    8. Start final baseline (30 min)
- Exporting data

    9. In data_and_results tab, select your experiment
    10. File -> export
    11. Ok everywhere, save full data, RESAMPLE data every 2 seconds
    12. Extract your csv


# Experimental flow
To be ready for calo: 
- Vial on tared precisa balance, one vial cap, red tool, small grey screw
- Cut pipette, paper towels
- Calo initialised and baseline finished

To be ready for rheo:
- IKA set up (mixing tool inserted, mixing speed already set, towels below IKA, stop watch ready)
- Il me faut ma poudre, ma flotte
- Une spatule a côté du rheo for the insertion
- Rheo initialised, fume hood on, water trap filled with distilled water
- _Rheo protocol properly set up_, experiment name already entered

Doing the experiment: 
1. Start the main part of calo measurement. At the same time start the stopwatch
2. Mix your sample for the defined time and speed (I used to do water in powder to be consistent with the phimax protocol but actually I can do powder to water at phimax and then go to phitarget. I just need to put the water in a bigger container.)
3. Prepare calo vial (if too high yield stress use very fine spatula, otherwise cut pipette does the job)
4. Insert calo vial, record insertion time
5. Put the rest of the sample in storage conditions (e.g. sealed with parafilm, water bath at 23C)
6. Prior to starting the rheo experiment, remix quickly by hand
7. Start rheo experiment. Record rheo press start and rheo actual start